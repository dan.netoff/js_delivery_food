export const cart = () => {
    const buttonCart = document.getElementById('cart-button')
    const modalCart = document.querySelector('.modal-cart')
    const body = modalCart.querySelector('.modal-body')
    const close = modalCart.querySelector('.close')
    const buttonSend = modalCart.querySelector('.button-primary')
    const pricetag = modalCart.querySelector('.modal-pricetag')
    const clearCart = modalCart.querySelector('.clear-cart')

    const resetCart = () => {
        body.innerHTML = ''
        localStorage.removeItem('cart')
        modalCart.classList.remove('is-open')
    }
    const incrementCount = (id) => {
        const cartArray = JSON.parse(localStorage.getItem('cart'))
        cartArray.map((item) => {
            if (item.id === id) {
                item.count++
            }
            return item
        })
        localStorage.setItem('cart', JSON.stringify(cartArray))
        renderItems(cartArray)
    }

    const decrementCount = (id) => {
        const cartArray = JSON.parse(localStorage.getItem('cart'))
        cartArray.map((item) => {
            if (item.id === id) {
                item.count = item.count > 0 ? item.count - 1 : 0
            }
            return item
        })
        localStorage.setItem('cart', JSON.stringify(cartArray))
        renderItems(cartArray)
    }

    body.addEventListener('click', (e) => {
        e.preventDefault()
        if (e.target.classList.contains('btn-inc')) {
            incrementCount(e.target.dataset.index)
        } else if (e.target.classList.contains('btn-dec')) {
            decrementCount(e.target.dataset.index)
        }
    })
    clearCart.addEventListener('click', (e) => {
        e.preventDefault()
        resetCart()
    })

    const renderItems = (data) => {
        body.innerHTML = ''
        data.forEach(cartItem => {
            const cartElem = document.createElement('div')
            cartElem.classList.add('food-row')
            cartElem.innerHTML = `
                <span class="food-name">${cartItem.name}</span>
                <strong class="food-price">${cartItem.price} $</strong>
                <div class="food-counter">
                    <button class="counter-button btn-dec" data-index="${cartItem.id}">-</button>
                    <span class="counter">${cartItem.count}</span>
                    <button class="counter-button btn-inc" data-index="${cartItem.id}">+</button>
                </div>
            `
            body.append(cartElem)
        })

        const res = data.reduce((sum, item)=>{
            return sum + (item.count * item.price)
        }, 0)
        pricetag.textContent = `${res} $`
    }


    buttonSend.addEventListener('click', (e) => {
        const cartArray = localStorage.getItem('cart')

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: cartArray,
        })
            .then(response => {
                if (response.ok) {
                    console.log('ok')
                }
            })
            .catch(e => {
                console.error(e)
            })
        resetCart()
    })

    buttonCart.addEventListener('click', () => {
        if (localStorage.getItem('cart')) {
            renderItems(JSON.parse(localStorage.getItem('cart')))
        } else {
            renderItems([])
        }
        modalCart.classList.add('is-open')
    })
    close.addEventListener('click', () => {
        modalCart.classList.remove('is-open')
    } )
}
