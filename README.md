# js_delivery_food



## Getting started
need install nodejs and npm:
for Ubuntu 20

```
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt-get install -y nodejs
node -v; npm -v
```

run node without sudo:
```
# append the following lines to the ~/.bashrc file
export NODE_HOME=~/nodejs-latest
export PATH=$PATH:$NODE_HOME/bin

# refresh environment variables
source ~/.bashrc
```

## Install WEBPACK


```
cd js_delivery_food
npm init -y
npm install webpack webpack-cli webpack-dev-server --save-dev

```

## new for me in js
```
# defer - run script after load page
<script src='1.js' defer></script>

# Стрелочные функции 
const my = () => {}

document.querySelector('.myClass').classlist.toggle('newClass')

# Деструктуризация
const {username, login, password} = myItem

#Dataset  датаатрибут
a.dataset.products = "palki-skalki.json"
<a href="/restaurant.html" class="card" data-products="palki-skalki.json">
</a>
```